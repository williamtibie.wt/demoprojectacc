﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ManagementWebApplication.Entities.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "employee",
                columns: table => new
                {
                    EmployeeId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employee", x => x.EmployeeId);
                });

            migrationBuilder.CreateTable(
                name: "employeeApp",
                columns: table => new
                {
                    EmployeeAppId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: false),
                    Url = table.Column<string>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true, defaultValue: "SYSTEM"),
                    CreatedAt = table.Column<string>(nullable: true, defaultValueSql: "Date('Now')"),
                    UpdatedAt = table.Column<string>(nullable: true, defaultValueSql: "Date('Now')"),
                    UpdatedBy = table.Column<string>(nullable: true, defaultValue: "SYSTEM"),
                    EmployeeId = table.Column<Guid>(nullable: false),
                    FK_Employee_EmployeeApp = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employeeApp", x => x.EmployeeAppId);
                    table.ForeignKey(
                        name: "FK_Employee_EmployeeApp",
                        column: x => x.EmployeeId,
                        principalTable: "employee",
                        principalColumn: "EmployeeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_employeeApp_EmployeeId",
                table: "employeeApp",
                column: "EmployeeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "employeeApp");

            migrationBuilder.DropTable(
                name: "employee");
        }
    }
}
