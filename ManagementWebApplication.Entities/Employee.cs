﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ManagementWebApplication.Entities
{
    public class Employee
    {
        [Key]
        public Guid EmployeeId { set; get; }
        [Required]
        public string Name { set; get; }
        [Required]
        public string Email { set; get; }
        [Required]
        public string PhoneNumber { set; get; }

        public List<EmployeeApp> EmployeeApps { set; get; }
    }
}
